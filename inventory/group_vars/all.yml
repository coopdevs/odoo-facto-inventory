---
# Use python3 interpreter
ansible_python_interpreter: "/usr/bin/python3"

# Odoo vars
odoo_role_odoo_user: odoo

odoo_role_odoo_core_modules_dict:
  shared:
    - account
    - account_analytic_default
    - account_asset
    - account_bank_statement_import
    - account_cancel
    - account_invoicing
    - analytic
    - auth_crypt
    - auth_signup
    - base
    - base_address_city
    - base_automation
    - base_iban
    - base_import
    - base_setup
    - base_vat
    - base_vat_autocomplete
    - board
    - calendar
    - contacts
    - crm
    - crm_project
    - decimal_precision
    - document
    - fetchmail
    - google_account
    - google_calendar
    - google_drive
    - google_spreadsheet
    - hr
    - hr_attendance
    - hr_contract
    - hr_expense
    - hr_holidays
    - hr_timesheet
    - l10n_es
    - mail
    - product
    - product_margin
    - project
    - purchase
    - resource
    - sale_crm
    - sale_expense
    - sale_management
    - sale_timesheet
    - sales_team
    - web
    - web_kanban_gauge
    - web_settings_dashboard
odoo_role_odoo_community_modules_dict:
  shared:
    - account_banking_mandate
    - account_banking_pain_base
    - account_banking_sepa_credit_transfer
    - account_banking_sepa_direct_debit
    - account_due_list
    - account_financial_report
    - account_payment_auto_partner_bank
    - account_payment_mode
    - account_payment_order
    - account_payment_partner
    - base_bank_from_iban
    - base_location
    - base_location_geonames_import
    - base_technical_features
    - contract
    - contract_sale
    - contract_variable_qty_timesheet
    - contract_variable_quantity
    - crm_timesheet
    - date_range
    - helpdesk_mgmt
    - helpdesk_mgmt_project
    - helpdesk_motive
    - helpdesk_type
    - l10n_es_account_asset
    - l10n_es_account_bank_statement_import_n43
    - l10n_es_account_invoice_sequence
    - l10n_es_aeat
    - l10n_es_aeat_mod111
    - l10n_es_aeat_mod115
    - l10n_es_aeat_mod123
    - l10n_es_aeat_mod130
    - l10n_es_aeat_mod216
    - l10n_es_aeat_mod296
    - l10n_es_aeat_mod303
    - l10n_es_aeat_mod347
    - l10n_es_aeat_mod349
    - l10n_es_aeat_mod390
    - l10n_es_aeat_partner_check
    - l10n_es_aeat_sii
    - l10n_es_facturae
    - l10n_es_facturae_efact
    - l10n_es_mis_report
    - l10n_es_partner
    - l10n_es_toponyms
    - l10n_es_vat_book
    - mail_force_sender
    - mass_editing
    - mis_builder
    - mis_builder_budget
    - mis_builder_cash_flow
    - project_category
    - project_task_default_stage
    - product_contract
    - project_timesheet_time_control
    - remove_dup_acc_num_constraint
    - report_xlsx
    - sale_order_invoicing_finished_task
    - web_decimal_numpad_dot
    - web_environment_ribbon
    - web_favicon
    - web_no_bubble
    - web_responsive
    - web_searchbar_full_width

odoo_role_download_strategy: tar
odoo_role_odoo_version: "12.0"
odoo_role_odoo_release: "12.0_2022-04-04"
odoo_role_odoo_url: "https://gitlab.com/coopdevs/OCB/-/archive/{{ odoo_role_odoo_release }}/OCB-{{ odoo_role_odoo_release }}.tar.gz"

odoo_role_list_db: true

# Odoo provisioning
odoo_provisioning_version: "v0.7.6"

# Nginx configuration
nginx_configs:
  upstream:
    - upstream odoo { server 127.0.0.1:8069; }
    - upstream nexporter { server 127.0.0.1:9100; }
    - upstream pexporter { server 127.0.0.1:9187; }

nginx_sites:
  odoo:
    - |
      listen 80;
      server_name {{ domains | default([inventory_hostname]) | join(' ') }};
      rewrite ^(.*)$ https://$host$1 permanent;
  odoo.ssl:
    - |
      listen 443 ssl;
      ssl_certificate /etc/letsencrypt/live/{{ inventory_hostname }}/fullchain.pem;
      ssl_certificate_key /etc/letsencrypt/live/{{ inventory_hostname }}/privkey.pem;
      include /etc/letsencrypt/options-ssl-nginx.conf;
      ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
      server_name {{ domains | default([inventory_hostname]) | join(' ') }};
      proxy_read_timeout 720s;
      proxy_connect_timeout 720s;
      proxy_send_timeout 720s;
      client_max_body_size 3000M;
      proxy_set_header X-Forwarded-Host $host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Real-IP $remote_addr;
      ssl on;
      access_log /var/log/nginx/odoo.access.log;
      error_log /var/log/nginx/odoo.error.log;
      location / {
        proxy_redirect off;
        proxy_pass http://odoo;
      }
      location /node/ {
        include proxy_params;
        proxy_redirect off;
        proxy_pass http://nexporter/metrics;
        auth_basic           "closed site";
        auth_basic_user_file /etc/odoo/.htpasswd;
      }
      location /postgres/ {
        include proxy_params;
        proxy_redirect off;
        proxy_pass http://pexporter/metrics;
        auth_basic           "closed site";
        auth_basic_user_file /etc/nginx/.pexporter.htpasswd;
      }
      gzip_types text/css text/less text/plain text/xml application/xml application/json application/javascript;
      gzip on;
